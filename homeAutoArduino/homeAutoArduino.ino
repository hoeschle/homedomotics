#include <RemoteTransmitter.h>

// Intantiate a new ActionTransmitter remote, use pin 9
ActionTransmitter actionTransmitter(9);
int signal[4];
int i = 0;


void setup() {
  Serial.begin(9600);
  Serial.flush();
}

void loop() {
  // enter in Serial Monitor 31A1 -> Switch on Action-device A on system code 31.
  // !!!!!! enter in Serial Monitor 05A1 -> Switch on Action-device A on system code 5. (it has to  be 4 ciffers/letters)
  if (Serial.available()>0){
    signal[i] = Serial.read();
    i++;
    if (i == 4){
      //Serial.println("New Signal");
      //Serial.println(signal[0]-48); // Zehnerstelle 
      //Serial.println(signal[1]-48); // Einerstelle
      //Serial.println(signal[2]); // device
      //Serial.println(signal[3]-48); // switch
      //Serial.println(signal[4]); //newline;
      int signalCode = (signal[0]-48)*10 + signal[1] - 48;
      char device = signal[2];
      int switchDevice = signal[3]-48;
      actionTransmitter.sendSignal(signalCode,device,switchDevice);      
      i=0;
    }
  }
}


<!DOCTYPE html> 
<html> 
<head> 
	<title>IEEE Home Automation</title> 
	<link rel="stylesheet" href="js/jquery.mobile-1.3.0.min.css" type="text/css">
	<script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
	<script src="js/jquery.mobile-1.3.0.min.js" type="text/javascript"></script>
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
</head> 
<body onload="loadProperties()"> 

<div data-role="page">
	<div data-role="header">
        <a href="index.php" data-transition="slide" data-icon="home">Home</a>
            <h1>Home Automation</h1>
        <a href="settings.php" data-transition="slide" data-ajax="true" rel="external" data-icon="gear">Settings</a>
	</div><!-- /header -->

	<div data-role="content">	
        <h2>Light switches</h2>
        <div data-role="fieldcontain">	
            <label for="A">A</label>
            <select name="sliderA" id="A" data-role="slider" data-mini="true">
                <option value="off">Off</option>
                <option value="on">On</option>
            </select>
        <div data-role="fieldcontain">	
            <label for="B">B</label>
            <select name="sliderB" id="B" data-role="slider" data-mini="true">
                <option value="off"">Off</option>
                <option value="on">On</option>
            </select>
        <div data-role="fieldcontain">	
            <label for="C">C</label>
            <select name="sliderC" id="C" data-role="slider" data-mini="true">
                <option value="off"">Off</option>
                <option value="on">On</option>
            </select>
        </div>
        <script>
            $('#A').change(function (event) {
                var mySwitch = event.currentTarget;
                var val = mySwitch.value == "on" ? 1 :0 ;
				$.post('updateProperties.php?key=switchState_A&val='+mySwitch.value, function(response){});
                $.post('sendControl.php?device='+mySwitch.id+'&switchDevice='+val, function (response){}); 
            });
            $('#B').change(function (event) {
                var mySwitch = event.currentTarget;
                var val = mySwitch.value == "on" ? 1 :0 ;
				$.post('updateProperties.php?key=switchState_B&val='+mySwitch.value, function(response){});
                $.post('sendControl.php?device='+mySwitch.id+'&switchDevice='+val, function (response){});  
            });
            $('#C').change(function (event) {
               var mySwitch = event.currentTarget;
                var val = mySwitch.value == "on" ? 1 :0 ;
				$.post('updateProperties.php?key=switchState_C&val='+mySwitch.value, function(response){});  
                $.post('sendControl.php?device='+mySwitch.id+'&switchDevice='+val, function (response){});				
            });

			function loadProperties(){
				
				$.getJSON('properties.json?'+ new Date().getTime(), function(data) {
					//alert ("I am here!");
					$('#A').val(data.switchState_A).slider('refresh');
					$('#B').val(data.switchState_B).slider('refresh');
					$('#C').val(data.switchState_C).slider('refresh');
	 			});
			}

		</script>
	</div><!-- /content -->

</div><!-- /page -->

</body>
</html>

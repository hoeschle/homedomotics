<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>
<html>
	<head> 
		<title></title> 
		<link rel="stylesheet" href="js/jquery.mobile-1.3.0.min.css" />
		<script src="js/jquery-1.8.2.min.js"></script>
		<script src="js/jquery.mobile-1.3.0.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
	</head>
	<body onload="loadProperties()">
		<div data-role="page" id="settings">
			<div data-role="header">
				<a href="index.php" data-transition="slide" data-icon="home">Home</a>
            	<h1>Home Automation Settings</h1>
			</div><!-- /header -->
			<div data-role="content">	
				<div data-role="collapsible" id="serialPort" data-collapsed="true">
		    		<h3>Serial Port</h3>
		    		<div data-role="fieldcontain">
						<label for="serialPortName">Serial Port:</label>
						<input type="text" name="name" id="serialPortName" value="write your serial port here..."  />
					</div>
				</div>
				<div data-role="collapsible" id="systemCode" data-collapsed="true">
		    		<h3>System Code</h3>
					<p>Set your system code here:</p>
					<?php 
						for ($i = 1; $i <= 5; $i++) {
		    				echo "<div data-role=\"fieldcontain\">";
								echo "<label for=\"flip_$i\">$i</label>";
								echo "<select name=\"flip_$i\" id=\"flip_$i\" data-role=\"slider\" data-mini=\"true\">";
									echo "<option value=\"off\">0</option>";
									echo "<option value=\"on\">1</option>";
								echo "</select>"; 
							echo "</div>";
						}?>
				</div>
			</div><!-- content -->
		</div><!-- page -->
	</body>
	<script>
		<?php
			for ($i = 1; $i <= 5; $i++) {	
				echo "$(\"#flip_$i\").change(function(event) {\n";
					echo "val = event.currentTarget.selectedIndex;\n";
					echo "$.post(\"updateProperties.php?key=flip_$i&val=\"+val, function(response){});\n";
				echo "});\n";
			}
		?>
		$('#serialPortName').change(function(event){
			//alert("Something changed");
			val = event.currentTarget.value;
			$.post("updateProperties.php?key=serialPort&val="+val, function(response){});
		});
		function loadProperties(){
			//alert ("I am here!");	
			$.getJSON('properties.json?'+ new Date().getTime(), function(data) {
  				$('#serialPortName').val(data.serialPort);
				<?php
					for ($i = 1; $i <= 5; $i++) {	
						echo "if (data.flip_$i == 1){\n";
							echo "$(\"#flip_$i\").val(\"on\").slider('refresh');\n";  
						echo "} else{\n";
							echo "$(\"#flip_$i\").val(\"off\").slider('refresh');\n";
						echo "}\n";
				}?>
 			});
		}
	</script>
</html>

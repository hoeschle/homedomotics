# HomeDomotics from IEEE Student Branch KU Leuven
 
## Walk-through

### Arduino

1. Download and install the software for the arduino from [Arduino][1]
2. Download the libraries (zip-file) for the RF Transmitter from [433 mhz for arduino][2]

2.a (Windows) Unzip and copy folder RemoteSwitch in libraries-folder of ardunio software

2.b (Linux) Unzip and copy folder RemoteSwitch to /usr/share/arduino/libraries

3. Upload homeAutoArduino to your arduino
4. Test your arduino software / plug configuration by sending commands via the Serial Monitor to the arduino

### Webserver
1. Download and install a webserver (XAMPP, LAMP, WAMP) suitable for your webserver
2. to be continued (see presentations)





### Presentations
The presentation can be used as guidelines, please be aware that the code-snippets in there
are not fully updated as the files in the repository

[1]: http://arduino.cc/en/Main/Software
[2]: https://bitbucket.org/fuzzillogic/433mhzforarduino/overview
